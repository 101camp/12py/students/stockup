import tushare as ts
import pandas as pd
from sqlalchemy import create_engine
import pymysql
import datetime
import time

host = '118.31.55.127'
port = 3306
db = 'stock'
user = 'stock'
password = 'stock'

engine = create_engine(f'mysql+pymysql://{user}:{password}@{host}:{port}/{db}', encoding='utf8')

#接口：new_share
#描述：获取新股上市列表数据
#限量：单次最大2000条，总量不限制，考虑分页提取(按年循环)
#接口每分钟最多访问10次

#today = datetime.date.today().strftime('%Y%m%d') # 今天日前
current_year = datetime.datetime.now().year # 今年年份

pro = ts.pro_api('8d4bad6681afc445b07392fe211710fdceb867b34c16220a4e146d49')
df_union = pd.DataFrame()
step = 10
for yr in range(2000, current_year+1, step):
    df = pro.new_share(start_date=f'{yr}0101', end_date=f'{yr+step-1}1231')
    print(f'循环年份：{yr}0101', f'{yr+step-1}1231')
    time.sleep(8)  # 接口每分钟最多访问10次，使用sleep延缓
    df_union = df_union.append(df)
pd.io.sql.to_sql(df_union, "new_share", engine, if_exists='replace', index=None)
