import yfinance as yf
from prettytable import PrettyTable

stock_name = "AAPL"

stock = yf.Ticker(stock_name)
stock_info = stock.info

# 遍历解析出所有信息
#for key,value in stock_info.items():
#    print(key+':'+str(value))

# 关键字段
key_list = ['dayHigh',
'dayLow',
'open',
'previousClose',
'fiftyDayAverage',
'fiftyTwoWeekHigh',
'fiftyTwoWeekLow',
'volume',
'trailingEps',
'bookValue',
'dividendRate',
'dividendYield',
'trailingPE',
'priceToBook',
'priceToSalesTrailing12Months',
'enterpriseValue',
'floatShares',
'sharesOutstanding',
'currency',
'longName',
'symbol',
'website',
'industry'
    ]
print("Information of stock")
table = PrettyTable(["key","value"])
table.align["Value"] = '1'
for key in key_list:
    table.add_row([key,stock_info[key]])
table.align = 'l'
print(table)
