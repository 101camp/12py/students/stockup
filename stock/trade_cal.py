import tushare as ts
import pandas as pd
from sqlalchemy import create_engine
import pymysql

host = '118.31.55.127'
port = 3306
db = 'stock'
user = 'stock'
password = 'stock'

engine = create_engine(f'mysql+pymysql://{user}:{password}@{host}:{port}/{db}', encoding='utf8')
pro = ts.pro_api()
df = pro.query('trade_cal')
pd.io.sql.to_sql(df, "trade_cal", engine, if_exists='replace', index=None)