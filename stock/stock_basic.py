import tushare as ts
import pandas as pd
from sqlalchemy import create_engine
import pymysql

host = '118.31.55.127'
port = 3306
db = 'stock'
user = 'stock'
password = 'stock'

engine = create_engine(f'mysql+pymysql://{user}:{password}@{host}:{port}/{db}', encoding='utf8')
pro = ts.pro_api()

#查询当前所有正常上市交易的股票列表
data = pro.stock_basic(exchange='', list_status='L', fields='ts_code,symbol,name,area,industry,fullname,enname,market,exchange,curr_type,list_status,list_date,delist_date,is_hs')

#写入数据，table_name为表名，‘replace’表示如果同名表存在就替换掉
pd.io.sql.to_sql(data, "stock_basic", engine, if_exists='replace')