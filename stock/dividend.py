import tushare as ts
import pandas as pd
from sqlalchemy import create_engine
import pymysql
import time

host = '118.31.55.127'
port = 3306
db = 'stock'
user = 'stock'
password = 'stock'

engine = create_engine(f'mysql+pymysql://{user}:{password}@{host}:{port}/{db}', encoding='utf8')

'''
接口：dividend
描述：分红送股数据
权限：用户需要至少300积分才可以调取，具体请参阅积分获取办法
注意：缺少base_date、base_share字段。每分钟最多访问该接口100次
'''

pro = ts.pro_api('8d4bad6681afc445b07392fe211710fdceb867b34c16220a4e146d49')
ts_code = pro.stock_basic(exchange='', list_status='L', fields='ts_code')
df_union = pd.DataFrame()
for index, row in ts_code.iterrows():
    print(row['ts_code'])
    df = pro.dividend(ts_code=row['ts_code'])
    df_union = df.append(df_union)
    time.sleep(0.62)
pd.io.sql.to_sql(df_union, "dividend", engine, if_exists='replace', index=None)
