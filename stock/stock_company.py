import tushare as ts
import pandas as pd
from sqlalchemy import create_engine
import pymysql

host = '118.31.55.127'
port = 3306
db = 'stock'
user = 'stock'
password = 'stock'

engine = create_engine(f'mysql+pymysql://{user}:{password}@{host}:{port}/{db}', encoding='utf8')
pro = ts.pro_api()

# 获取上市公司基础信息
# - API提醒单次提取4000条，可以根据交易所分批提取(实际发现可一次性获取4000条)
# - 字段顺序和实际入库顺序有差异？

exchange = ['SZSE', 'SSE']
fields='ts_code,exchange,chairman,manager,secretary,reg_capital,setup_date,province,city,introduction,website,email,office,employees,main_business,business_scope'
df_union = pd.DataFrame()
for i in exchange:
    df = pro.stock_company(exchange=i, fields=fields)
    df_union=df_union.append(df)
pd.io.sql.to_sql(df_union, "stock_company", engine, if_exists='replace', index=None)
