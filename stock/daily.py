import tushare as ts
import pandas as pd
from sqlalchemy import create_engine
import pymysql
import datetime
import time

host = '118.31.55.127'
port = 3306
db = 'stock'
user = 'stock'
password = 'stock'

engine = create_engine(f'mysql+pymysql://{user}:{password}@{host}:{port}/{db}', encoding='utf8')

'''
接口：daily
数据说明：交易日每天15点～16点之间。本接口是未复权行情，停牌期间不提供数据。
调取说明：基础积分每分钟内最多调取500次，每次5000条数据，相当于23年历史，用户获得超过5000积分正常调取无频次限制。
描述：获取股票行情数据，或通过通用行情接口获取数据，包含了前后复权数据。
'''

today = datetime.date.today().strftime('%Y%m%d') # 今天日前
#current_year = datetime.datetime.now().year # 今年年份

pro = ts.pro_api('8d4bad6681afc445b07392fe211710fdceb867b34c16220a4e146d49')
df_union = pd.DataFrame()
step = 1
for day in range(20200101, int(today)+1, step):
    df = pro.daily(trade_date= day)
    print(f'日期：{day}')
    time.sleep(0.14)  # 接口每分钟最多访问500次，使用sleep延缓
    df_union = df_union.append(df)
pd.io.sql.to_sql(df_union, "daily", engine, if_exists='replace', index=None)
