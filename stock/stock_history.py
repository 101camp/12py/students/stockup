import yfinance as yf
import pandas as pd
from pandas_datareader import data as pdr

# get historical market data

stock = ["AAPL"]
interval_type = ['1d', '1wk', '1mo', '3mo']
yf.pdr_override()
print("get historical market data，the data is stored on ../data")
for i in interval_type:
    df_union = pd.DataFrame()  # 初始化空数据框
    for j in stock:
        data = pdr.get_data_yahoo(j, interval=i).assign(ticker=j)
        df_union = df_union.append(data)  # 数据框合并
        #data.to_csv(f'../data/stock_{j}_{i}.csv') # 按股票-间隔类型存文件
    df_union.to_csv(f'../data/stock_{i}.csv')  # 按间隔类型存文件
