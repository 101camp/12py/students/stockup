import tushare as ts
import pandas as pd
from sqlalchemy import create_engine
import pymysql

host = '118.31.55.127'
port = 3306
db = 'stock'
user = 'stock'
password = 'stock'

engine = create_engine(f'mysql+pymysql://{user}:{password}@{host}:{port}/{db}', encoding='utf8')
pro = ts.pro_api()

# 获取沪股通、深股通成分数据
hs_type = ['SH', 'SZ']
df_union = pd.DataFrame()
for i in hs_type:
    df = pro.hs_const(hs_type=i)
    df_union=df_union.append(df)
pd.io.sql.to_sql(df_union, "hs_const", engine, if_exists='replace', index=None)
