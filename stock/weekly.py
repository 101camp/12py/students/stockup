import tushare as ts
import pandas as pd
from sqlalchemy import create_engine
import pymysql
import datetime
import time

host = '118.31.55.127'
port = 3306
db = 'stock'
user = 'stock'
password = 'stock'

engine = create_engine(f'mysql+pymysql://{user}:{password}@{host}:{port}/{db}', encoding='utf8')

'''
接口：weekly
描述：获取A股周线行情
限量：单次最大4500行，总量不限制
积分：用户需要至少300积分才可以调取，具体请参阅积分获取办法
'''

today = datetime.date.today().strftime('%Y%m%d') # 今天日前
#current_year = datetime.datetime.now().year # 今年年份

pro = ts.pro_api('8d4bad6681afc445b07392fe211710fdceb867b34c16220a4e146d49')
df_union = pd.DataFrame()
step = 1
for wk in range(20200101, int(today)+1, step):
    if int(str(wk)[-2:])<=31:
        #print(str(wk)[-2:]) 截取日期后两位，判断日期小于31，防止32-99之间的数字循环
        df = pro.weekly(trade_date= wk)
        print(f'{wk}')
        time.sleep(0.32)  # 接口每分钟最多访问200次，使用sleep延缓
        df_union = df_union.append(df)
pd.io.sql.to_sql(df_union, "weekly", engine, if_exists='replace', index=None)
