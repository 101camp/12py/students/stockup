import tushare as ts
import pandas as pd
from sqlalchemy import create_engine
import pymysql
import datetime
import time

host = '118.31.55.127'
port = 3306
db = 'stock'
user = 'stock'
password = 'stock'

engine = create_engine(f'mysql+pymysql://{user}:{password}@{host}:{port}/{db}', encoding='utf8')

'''
接口：daily_basic
更新时间：交易日每日15点～17点之间
描述：获取全部股票每日重要的基本面指标，可用于选股分析、报表展示等。
积分：用户需要至少300积分才可以调取，具体请参阅积分获取办法
'''

today = datetime.date.today().strftime('%Y%m%d') # 今天日前
#current_year = datetime.datetime.now().year # 今年年份

pro = ts.pro_api('8d4bad6681afc445b07392fe211710fdceb867b34c16220a4e146d49')
#df_union = pd.DataFrame()
step = 1
#int(today)+1
for day in range(20000101, 20041232, step):
    if int(str(day)[-2:])<=31 and int(str(day)[4:6])>=1 and int(str(day)[4:6])<=12:
        #print(int(str(day)[4:6]))
        #break
        df = pro.daily_basic(trade_date= day)
        print(f'日期：{day}')
        time.sleep(0.31)  # 接口每分钟最多访问200次，使用sleep延缓
        #df_union = df_union.append(df)
        pd.io.sql.to_sql(df, "daily_basic", engine, if_exists='append', index=None)
#pd.io.sql.to_sql(df_union, "daily_basic", engine, if_exists='replace', index=None)
