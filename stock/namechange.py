import tushare as ts
import pandas as pd
from sqlalchemy import create_engine
import pymysql

host = '118.31.55.127'
port = 3306
db = 'stock'
user = 'stock'
password = 'stock'

engine = create_engine(f'mysql+pymysql://{user}:{password}@{host}:{port}/{db}', encoding='utf8')
pro = ts.pro_api()
df = pro.namechange(ts_code='', fields='ts_code,name,start_date,end_date,ann_date,change_reason')
pd.io.sql.to_sql(df, "namechange", engine, if_exists='replace', index=None)