import json

# @task
# def stockup_html(s):

new_file = open('html/head.txt','r')
head = new_file.read()
new_file.close()
# print(head)

# Give a value to it 
# OR open a file with value in it 
# OR use "input" to ask user type in a name
company_name = "Apple Inc."

new_file = open('html/pre_pre.txt','r')
pre_pre = new_file.read()
new_file.close()
# print(pre_pre)

# get company's basic stock info from a specific folder and file
new_file = open('data/stock_info.txt','r')
stock_info = new_file.read()
new_file.close()
# print(stock_info)

new_file = open('html/post_pre.txt','r')
post_pre = new_file.read()
new_file.close()
# print(post_pre)

"""# read json file for the stock plot url
plotURL_file = open('data/plotURL_json.txt','r')
plot_url = json.load(str(plotURL_file))
# print(type(plotURL_file))
plotURL_file.close()
# print(type(plot_url))
# print(plot_url)"""

# read json file for the stock plot url
with open('data/plotURL_json.txt') as json_file:
    plot_url = json.load(json_file)

tablink_list = []
tabcontent_list = []

# create tablinks from json file
for i in plot_url:
    # print(i)
    newline = f'''<button class="tablinks" onclick="openStockGraphics(event, '{i}')">{i}</button>\n'''
    tablink_list.append(newline)
    # print(newline)

    # with open('html/tablink.txt', 'w') as tl:
        # tl.writelines(tab)

tablink = "".join(tablink_list)
# print(tablink)

new_file = open('html/tab.txt','r')
tab = new_file.read()
new_file.close()

# create tabcontent from json file
for i in plot_url:
    newline = f'''<div id="{i}" class="tabcontent"><h3>{i}</h3>{plot_url[i]}</div>\n'''
    tabcontent_list.append(newline)

    # with open('html/tabcontent.txt', 'w') as tc:
        # tc.writelines(tab)

tabcontent = "".join(tabcontent_list)
# print(tabcontent)

new_file = open('html/ending.txt','r')
ending = new_file.read()
new_file.close()

company_test = head + company_name + pre_pre + stock_info + post_pre + tablink + tab + tabcontent + ending
with open('public/category/company.html','w') as company:
    company.write(company_test)
