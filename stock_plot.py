import pandas as pd
import numpy as np


#import os
import matplotlib.pyplot as plt



df = pd.read_csv('data/stock_1wk.csv')

# plot---------------------------------------------------------

fig, (ax1, ax2) = plt.subplots(2,1,figsize=(70,30))
fig.subplots_adjust(hspace=0.5)
fig.suptitle('1 Year Stock Price', fontsize=50, fontweight='bold')


ax1.plot(df.iloc[2099:2158,0],df.iloc[2099:2158,2], color='red', marker='+', linestyle='solid',linewidth=4, markersize=2,label='high')
ax1.plot(df.iloc[2099:2158,0],df.iloc[2099:2158,3], color='green', marker='+', linestyle='solid',linewidth=4, markersize=2,label='low')
ax1.set_xlabel('Date',fontsize = 50.0)
ax1.set_ylabel('high and low',fontsize = 50.0)
ax1.legend(loc='upper right',fontsize = 40.0)
ax1.grid(True)
ax1.text(10, 120, 'AAPL', style='italic', fontsize = 50,
        bbox={'facecolor': 'yellow', 'alpha': 0.5, 'pad': 10})



ax2.plot(df.iloc[2099:2158,0],df.iloc[2099:2158,4], color='blue', marker='+', linestyle='solid',linewidth=4, markersize=2,label='close')
ax2.set_xlabel('Date',fontsize = 50.0)
ax2.set_ylabel('Close',fontsize = 50.0)
ax2.legend(loc='upper right',fontsize = 40.0)
ax2.grid(True)
ax2.text(10, 120, 'AAPL', style='italic', fontsize = 50,
        bbox={'facecolor': 'yellow', 'alpha': 0.5, 'pad': 10})


plt.rc('xtick', labelsize=10)
plt.rc('ytick', labelsize=35)

plt.show()
fig.savefig('1_year_2019-current.jpeg')

# plot---------------------------------------------------------