from yahoo_fin.stock_info import get_data
import yahoo_fin.stock_info as si
import pprint as pp
from matplotlib import pyplot as plt

import pandas as pd

stock_name = 'aapl'  # 股票代码
stock_day = get_data(stock_name, start_date=None, end_date=None, index_as_date=True, interval="1d")
stock_day.to_csv('./data/stock_day.csv')
#print(stock_day)

stock_weekly = get_data(stock_name, start_date=None, end_date=None, index_as_date=True, interval="1wk")
stock_weekly.to_csv('./data/stock_weekly.csv')
#print(stock_weekly)
#print(stock_weekly.columns)

stock_month = get_data(stock_name, start_date=None, end_date=None, index_as_date=True, interval="1mo")
stock_month.to_csv('./data/stock_month.csv')
#print(stock_month)

# 获取市盈率PE(Price to Earnings Ratio)
quote_table = si.get_quote_table(stock_name, dict_result=True)
#pp.pprint(quote_table)

#字典转DataFrame
quote_table_df = pd.DataFrame.from_dict(quote_table,orient='index')
print(quote_table_df)
quote_table_df.to_csv('./data/quote_table.txt', sep=':', header=None)

print(si.get_stats_valuation(stock_name))

income_statement = si.get_income_statement(stock_name)
income_statement.to_csv('./data/income_statement.csv')
print(income_statement)

# 资产负债表数据
balance_sheet = si.get_balance_sheet(stock_name)
print(balance_sheet)
